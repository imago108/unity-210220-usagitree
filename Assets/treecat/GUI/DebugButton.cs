﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugButton : MonoBehaviour
{
    public void OnClick_ScreenRes_FHD() {
        Debug.Log("Screen Res -> 1920x1080");
        Screen.SetResolution(1920, 1080, FullScreenMode.Windowed);

  }
}
