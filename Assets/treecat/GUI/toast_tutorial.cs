﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class Subtasks
{
    public string title;
    public bool bChecked;
}

public class toast_tutorial : MonoBehaviour
{
    public Text textTitle;
    public Text textDescription;
    public GameObject subTaskVerticalBox;
    public GameObject guiSubTask;

    private List<Subtasks> subTasks = new List<Subtasks>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetTitle(string title)
    {
        if (!textTitle)
        {
            return;
        }
        textTitle.text = title;
    }

    public void SetDescription(string description)
    {
        if (!textDescription)
        {
            return;
        }
        textDescription.text = description;
    }

    public void addSubTask(string title)
    {
        Subtasks newSubTask = new Subtasks();
        newSubTask.title = title;
        newSubTask.bChecked = false; 
        subTasks.Add(newSubTask);

        // make sub tasks gui
        var a = Instantiate(guiSubTask, subTaskVerticalBox.transform);
    }

    public bool checkSubTask(string title)
    {
        return false;

    }
}
