﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveIkachang : MonoBehaviour
{
    private float timer = 0.0f;

    public Material body_mat;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        this.transform.position = new Vector3(2.0f, Mathf.Sin(timer) + 10.0f, 10.0f);

        // update material
        float sin_from_timer = ( Mathf.Sin(timer * 0.02f) + 1.0f ) / 2.0f;
        float pig_res = sin_from_timer * 1.3f + 0.3f ; // 0.8 ~ 2.5
        body_mat.SetFloat("_PigmmentResolution", pig_res);

        sin_from_timer = ( Mathf.Sin(timer * 1.0f) + 1.0f ) / 2.0f;
        float pig_power = 1f - (sin_from_timer * 10f) - 0.2f;
        body_mat.SetFloat("_PigmentPow", pig_power);


        
    }
}
