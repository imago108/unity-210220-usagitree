﻿Shader "Unlit/IkaBase"
{
    Properties
    {
        // Properties: Color
        _ColorA ("Color A", Color) = (0.0, 0.0, 0.0, 1.0)
        _ColorB ("Color B", Color) = (0.18, 0.18, 0.18, 1.0)
        _ColorC ("Color C", Color) = (1.0, 1.0, 1.0, 1.0)
        _ColorCenter ("Color Center Value", Float) = 0.5

        _Precision( "Precision", Float ) = 40.0
        _PigmmentResolution( "Pigment Resolution", float ) = 10.0
        _PigmentPow( "Pigment Power", Float ) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM

            #include "UnityCG.cginc"

            #pragma vertex vert
            #pragma fragment frag

            struct appdata
            {
                float4 vertex : POSITION; // Object-space position
                float3 normal: NORMAL; //
                float4 cd: COLOR; // vertex color
            };

            // フラグメントシェーダーで使用する構造体
            struct v2f
            {
                float4 vertex : SV_POSITION;
                nointerpolation fixed4 color: COLOR0;
                float4 cd: COLOR1;
                float4 object_pos : COLOR2;
            };

            float4 _ColorA;
            float4 _ColorB;
            float4 _ColorC;
            float _ColorCenter;
            float _Precision;
            float _PigmmentResolution;
            float _PigmentPow;

            v2f vert (appdata v)
            {
                // 座標変換を行う
                float3 viewpos = UnityObjectToViewPos(v.vertex);
                viewpos = floor(viewpos * _Precision) / _Precision;

                float4 sp = mul(UNITY_MATRIX_P, float4(viewpos, 1.0));

                // フラグメントシェーダーに渡す構造体を用意する
                v2f o;
                
                o.vertex = sp;
                // Vertex Color をそのまま渡す
                o.cd = v.cd;

                // Object-Space の Positionを代入
                o.object_pos = v.vertex;

                o.color.xyz = v.normal;
                // o.color.xyz = v.vertex;
                o.color.w = 1;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // イカの色素の分布を取得 (0 ~ 1)
                float map_pigment = i.cd.r;
                // 色素分布をパラメーターから調整する
                map_pigment = pow( map_pigment, ( 1.0 - _PigmentPow) );

                // 今の色素の強さを取得 (0 ~ 1)
                float strength_pigment = 0.5;

                // 色素のグリッドレゾリューション
                float res_pigment = _PigmmentResolution * 0.01;

                // オブジェクトスペースのポジション
                float3 obj_pos = i.object_pos.xyz;
                // Add Time
                obj_pos += float3(_Time.x, _Time.x, _Time.x) / 10.0;

                // 量子化した色素用の座標
                float3 quantum_pos = ( obj_pos % res_pigment) / res_pigment;
                quantum_pos = abs(quantum_pos);

                // 量子化するためのセンター
                float3 pigment_center = float3(0.5, 0.5, 0.5);

                // 色素細胞をつくる
                float pig = distance(quantum_pos, pigment_center);

                // 色素細胞のマスク を作る
                float mask_pig = pig * 3.0 / map_pigment;
                mask_pig = floor(mask_pig);
                pig = saturate(pig * mask_pig);
                pig = 1.0 - pig;

                // pig = pig * map_pigment;

                // 色を作る
                float color_alpha = pig;
                float blend_color_a = 1.0 - saturate( color_alpha / _ColorCenter ); 
                float blend_color_b = 1.0 - abs(1 - ( color_alpha / _ColorCenter ));
                float blend_color_c = saturate(color_alpha - _ColorCenter) / (1 - _ColorCenter);


                fixed4 col = blend_color_a * _ColorA +
                             blend_color_b * _ColorB +
                             blend_color_c * _ColorC;

                // fixed4 col = float4(pig, 0.0, 0.0, 1.0);
        
                return col;
            }
            ENDCG
        }
    }
}
