﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Item
{

[CreateAssetMenu]
public class ItemData: ScriptableObject
{

    [SerializeField]
    private string itemId;

    [SerializeField]
    private string itemName;

    [SerializeField]
    private string itemDescription;

    [SerializeField]
    private Texture itemIcon;
    
    public string GetItemId()
    {
        return this.itemId;
    }

}

}