﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Item
{

[CreateAssetMenu]
public class ItemDatabase : ScriptableObject
{
    [SerializeField]
    private List<ItemData> itemLists = new List<ItemData>();

    public List<ItemData> GetItemLists()
    {
        return this.itemLists;
    }
}

}