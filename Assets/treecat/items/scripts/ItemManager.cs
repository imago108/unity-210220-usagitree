﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Item
{

public class ItemManager : MonoBehaviour
{
    [SerializeField]
    private ItemDatabase itemDatabase;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public ItemData GetItemFromId(string itemId) {
        return this.itemDatabase.GetItemLists().Find(e => e.GetItemId() == itemId);

    }
}

}