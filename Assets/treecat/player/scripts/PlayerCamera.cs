﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField]
    public GameObject target;

    [SerializeField]
    private float length = 10.0f;


    [SerializeField]
    private float positionInterpSpeed = 10.0f;

    // Start is called before the first frame update
    void Start()
    {

        
    }

    void LateUpdate()
    {
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll != 0.0f)
        {
            this.length += scroll * 10 * -1;
        }

        // 
        var forward = this.transform.forward;
        var goal_position = target.transform.position + forward * (0 - length);
        this.transform.position = Vector3.Lerp(
            this.transform.position,
            goal_position,
            this.positionInterpSpeed * Time.deltaTime
        );
        
    }
}
