﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{

    public float speed = 10.0f;

    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        var InputVector = new Vector3(
            Input.GetAxis("Horizontal"),
            0,
            Input.GetAxis("Vertical")
        );

        var deltaVector = InputVector.magnitude * InputVector.normalized * this.speed * Time.deltaTime;
        transform.position += deltaVector;
        
        // Update Animator
        if (animator)
        {
            animator.SetFloat("speed", deltaVector.magnitude);
        }
    }
}
