﻿Shader "Unlit/StaticBase"
{
    Properties
    {
        _MainColor ("Color A", Color) = (0.18, 0.18, 0.18, 1)
        _MainColorTwo ("Color B", Color) = (0.18, 0.18, 0.18, 1)
        _Seed ("Seed", Int) = 1
        _Precision( "Precision", float ) = 40.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM

            #include "UnityCG.cginc"

            #pragma vertex vert
            #pragma fragment frag

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal: NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                nointerpolation fixed4 color: COLOR0;
            };

            float4 _MainColor;
            float4 _MainColorTwo;
            int _Seed;
            float _Precision;

            float GetRandomNumber(float3 pos, int seed)
            {
                return frac(sin(dot(pos, float3(12.9898, 78.233, 64.3456)) + seed) * 43758.5453);
            }

            v2f vert (appdata v)
            {
                float3 viewpos = UnityObjectToViewPos(v.vertex);
                viewpos = floor(viewpos * _Precision) / _Precision;

                float4 sp = mul(UNITY_MATRIX_P, float4(viewpos, 1.0));

                v2f o;
                // float4 temp = floor( v.vertex * _Precision ) / _Precision;
                // o.vertex = UnityObjectToClipPos(temp);
                
                o.vertex = sp;

                o.color.xyz = v.normal;
                // o.color.xyz = v.vertex;
                o.color.w = 1;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                float alpha = GetRandomNumber(i.color.xyz, _Seed);
                fixed4 col = _MainColor * (1 - alpha) + _MainColorTwo * alpha;
        
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
