﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task
{


[CreateAssetMenu(menuName="Tasks/Task")]
public class TaskData : ScriptableObject
{
    [SerializeField]
    private string taskId;
    
    [SerializeField]
    private string taskTitle;
    
    [SerializeField]
    private string taskDescription;
    
    [SerializeField]
    private List<string> subTasks;

}

}