﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task
{

[CreateAssetMenu(menuName="Tasks/TaskDatabase")]
public class TaskDatabase : ScriptableObject
{
    [SerializeField]
    private List<TaskData> taskLists = new List<TaskData>();

    public List<TaskData> GetTaskLists()
    {
        return this.taskLists;
    }
}

}