#!/usr/bin/env bash

xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' \
  unity-editor \
    -logFile /dev/stdout \
    -batchmode \
    -nographics \
    -createManualActivationFile |
      tee ./unity-output.log

exit 0